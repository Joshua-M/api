require "test_helper"

class UserTest < Minitest::Test

  def setup
    @user = FactoryGirl.create(:user)
  end

  def test_user_should_be_valid
    assert @user.valid?
  end
end
