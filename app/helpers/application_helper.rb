module ApplicationHelper
  include SessionsHelper

  def exists?(obj)
    obj.nil?
  end
end