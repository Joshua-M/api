module SessionsHelper
  
  def create_token(data = {})
    JWT.encode(
      data,
      Rails.application.secrets.secret_key_base,
      'HS256'
    )
  end
end