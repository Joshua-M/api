class V1::SessionsController < ApplicationController

  def create
    @user = User.find_by(email: params[:email])
    if @user&.valid_password?(params[:password])
      render :create, locals: {token: create_token({id: @user.id})}, status: :ok
    else
      head(:unauthorized)
    end
  end

  def destroy
  end
end
