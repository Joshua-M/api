class V1::UsersController < ApplicationController
  def index
    users = User.all
    render json: users, status: :ok
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: :created
    else
      head(:unprocessable_entity)
    end
  end

  def show
    user = User.find(params[:id])
    if user
      render json: user, status: :ok
    else
      head(:not_found)
    end
  end

  def update
    user = User.find(params[:id])
    if !user
      head(:not_found) and return
    end
    if user.update(user_params)
      render json: user, status: :ok
    else
      head(:unprocessable_entity)
    end
  end

  def destroy
    @user = User.where(id: params[:id]).first
    if !@user
      head(:not_found) and return
    end
    if @user.destroy
      render head: :ok
    else
      head(:unprocessable_entity)
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
