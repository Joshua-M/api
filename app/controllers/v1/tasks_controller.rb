class V1::TasksController < ApplicationController
  before_action :set_user

  def index
    render json: @user.tasks
  end

  def create
    if (task = @user.tasks.create!(task_params))
      render json: task, status: :created
    else
      head(:unprocessable_entity)
    end
  end

  def show
    task = Task.find(params[:id])
    head(:not_found) and return if exists?(task)
    render json: task, status: :ok
  end

  def update
    task = Task.find(params[:id])
    if !task
      head(:not_found) and return
    end
    if task.update(task_params)
      render json: task, status: :ok
    else
      head(:unprocessable_entity)
    end
  end

  def destroy
    task = Task.find(params[:id])
    if !task
      head(:not_found)
    end
    if task.destroy
      head(:ok)
    else
      head(:unprocessable_entity)
    end
  end

  private
  
  def set_user
    @user = User.first
  end

  def task_params
    params.require(:task).permit(:description)
  end
end
